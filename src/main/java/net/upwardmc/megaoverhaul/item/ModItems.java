/*
 * This file is part of MegaOverhaul.
 *
 * MegaOverhaul is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * MegaOverhaul is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with MegaOverhaul. If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * This file is part of MegaOverhaul.
 *
 * MegaOverhaul is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * MegaOverhaul is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with MegaOverhaul. If not, see <https://www.gnu.org/licenses/>.
 */

package net.upwardmc.megaoverhaul.item;

import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.upwardmc.megaoverhaul.MegaOverhaul;

public final class ModItems
{
    private final IEventBus modEventBus;
    private final DeferredRegister<Item> items;

    public ModItems(final IEventBus modEventBus)
    {
        this.modEventBus = modEventBus;
        this.items = DeferredRegister.create(ForgeRegistries.ITEMS, MegaOverhaul.MOD_ID);
        this.registerBrokenToolItems();
    }

    private void registerBrokenToolItems()
    {
        this.registerBrokenToolItem("diamond_pickaxe_broken");
        this.registerBrokenToolItem("netherrite_pickaxe_broken");
    }

    private void registerBrokenToolItem(final String name) {
        this.items.register(name, () -> new BrokenToolItem(this.modEventBus, new Item.Properties()));
    }

    public void register()
    {
        this.items.register(this.modEventBus);
    }
}
