/*
 * This file is part of MegaOverhaul.
 *
 * MegaOverhaul is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * MegaOverhaul is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with MegaOverhaul. If not, see <https://www.gnu.org/licenses/>.
 */

package net.upwardmc.megaoverhaul.item;

import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.eventbus.api.IEventBus;

import java.util.Objects;

public class BrokenToolItem extends Item
{
    public BrokenToolItem(final IEventBus eventBus, final Properties pProperties)
    {
        super(pProperties);
        eventBus.addListener(this::addCreativeTab);
    }

    private void addCreativeTab(CreativeModeTabEvent.BuildContents event)
    {
        if (!Objects.equals(event.getTab(), CreativeModeTabs.TOOLS_AND_UTILITIES))
        {
            return;
        }

        event.accept(this);
    }
}
