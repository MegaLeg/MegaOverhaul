/*
 * This file is part of MegaOverhaul.
 *
 * MegaOverhaul is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * MegaOverhaul is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with MegaOverhaul. If not, see <https://www.gnu.org/licenses/>.
 */

package net.upwardmc.megaoverhaul;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.upwardmc.megaoverhaul.item.ModItems;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(MegaOverhaul.MOD_ID)
public final class MegaOverhaul
{
    // Define mod id in a common place for everything to reference
    public static final String MOD_ID = "megaoverhaul";
    // Directly reference a slf4j logger

    public MegaOverhaul()
    {
        IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();

        ModItems modItems = new ModItems(modEventBus);
        modItems.register();

        // Register the commonSetup method for modloading
        modEventBus.addListener(this::commonSetup);

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }

    private void commonSetup(final FMLCommonSetupEvent event)
    {
    }
}
