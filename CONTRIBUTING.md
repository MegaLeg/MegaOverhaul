# Contributing to MegaOverhaul
This file is subject to change as work is needed. If you'd like to contribute but don't see anything
you feel you could help with, check back later, as something you can help with may be added.
This also is not a comprehensive requirement for submitting a Merge Request(MR), as in MRs do not necessarily
need to relate to something on this list. Generally speaking, all MRs are welcome.

## Needed Ideas
Feedback is currently needed for which features in the base game cause particular annoyance or cause
major game progression altering balance concerns.

## Needed Assets
If an asset is contributed and accepted, credits will be placed in the CREDITS file
### Textures
Textures are needed for broken variants of iron, diamond, and netherrite tools/armor.

### UI Elements
Certain blocks, such as the anvil will have their behavior changed in ways that the UI needs to reflect,
such as an additional slot for resources, etc. New UIs that visually accept new items are needed.
A list of all new UIs and what is needed will be below and expanded as additional work is done:

Anvil - Needs to accept iron ingots in addition to the XP requirement.

### Images
Images are needed for the master Git repository as well as the in-game logo.

## How to Contribute Code
1. Setup environment:
   ==============================

   Step 1: Open your command-line and browse to the folder where you extracted the zip file.

   Step 2: You're left with a choice.
   If you prefer to use Eclipse:
    a) Run the following command: `gradlew genEclipseRuns` (`./gradlew genEclipseRuns` if you are on Mac/Linux)
    b) Open Eclipse, Import > Existing Gradle Project > Select Folder or run `gradlew eclipse` to generate the project.

   If you prefer to use IntelliJ:
    a) Open IDEA, and import project.
    b) Select your build.gradle file and have it import.
    c) Run the following command: `gradlew genIntellijRuns` (`./gradlew genIntellijRuns` if you are on Mac/Linux)
    d) Refresh the Gradle Project in IDEA if required.

   If at any point you are missing libraries in your IDE, or you've run into problems you can
   run `gradlew --refresh-dependencies` to refresh the local cache. `gradlew clean` to reset everything
   {this does not affect your code} and then start the process again.

2. Create changes:
   ==============================
    Create whatever changes you wanted, keeping in mind the design philosophy and code style formatting.

3. Submit for review:
   ==============================
    Commit your changes to a forked repo and create a Merge Request. Be sure to include an accurate and thorough
    title and description. Avoid making substantial changes and committing as one enormous commit.
    Rather, make simple to understand commits and push them to your fork before opening a Merge Request.
    If your changes are accepted, you will be credited in CREDITS.
