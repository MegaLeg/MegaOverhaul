# MegaOverhaul

## Overview
MegaOverhaul attempts to do two main things:
1. Undo some questionable design/balance decisions that have been made in the past, such as enchantment tables
only taking the amount of Lapis needed, or anvils being seemingly random in their cost, with things
such as the order in which enchantments are applied mattering.
2. Fix the late-game stage, which has been lacking for a long time with hardly anything to do
gameplay-wise, and even when there is content present, it often leaves you underwhelmed. This is
either caused by lackluster pushover mechanics, such as the ender dragon sitting still for an
extended period of time allowing you to easily wail on her, or combat exploits that have remained un-patched
since their inception, such as bed-cheesing the ender dragon, or trapping the Wither in bedrock on spawn. 

## Download
Instructions for downloading MegaOverhaul to be added later.

## Design Philosophy

Balance changes are intended to fix the most egregious balance errors,
while allowing both intrinsically and extrinsically motivated players to enjoy
the gameplay experience and maintain the Vanilla environment.

MegaOverhaul does not contain purely nerfs. Certain things, such as beacons, will be buffed as the amount
of effort required to obtain them dwarfs the extrinsic reward that is presented, whereas intrinsically
it is understood the effort that was put in just from seeing a beacon, which would be enough for those players.

MegaOverhaul's design philosophy towards challenge aims to fit into Minecraft's choice for challenge philosophy.
As in, challenge will typically not present itself, it must be sought out intentionally.

With that being said, if challenge is to present itself, it must aim to support the actions of the player. 
For example, a mob that spawns with a feral quality, knowing exactly how to get to the player and blows up
a section of their build, does not demonstrate support of the player. For extrinsically motivated players,
this thrill may be desired, in which case they may **choose** challenge, just through different means.
For intrinsically motivated players, this may feel unfair and not fun, especially if this was never the intended
style of gameplay they were looking for from playing. Instead, mechanics such as explosions should be directly
caused by the player in more ways than the player simply existing in a particular location.

Instead, a mob may have a lunge mechanic, whose lunge can only be triggered by a player attempting to attack first.
This causes the dynamic system of removing and placing blocks to not be encouraged necessarily through survival means,
as generally most house-like structures created by players will tend to keep vanilla mobs out. This causes
generally anything a player builds to be a "valid" defense structure so long as a mob cannot simply walk to the player,
which should be a pretty self-explanatory system. However, if a player wishes to build defensively,
they may do so and be intrinsically rewarded with more peace of mind during times of attack,
or extrinsically motivated with whatever loot is dropped from dealing with mobs. The lunge mechanic applies only here,
as attempting to attack these mobs will cause them to enter an aggression state, where abilities such as lunge
become usable and test your defensive structures.

This appears to be a great compromise, as extrinsic players may enjoy the combat or seek the loot dropped, while
intrinsic players can simply ignore the challenge, even when it presents itself, such as an event, like a blood moon,
by just having any form of remote structure built at all since mobs will not penetrate unless actively attacked.

## Contributing
See the CONTRIBUTING file for information on what is currently needed in the project
as well as how to contribute.

## Building Instructions
In order to build a jar from source, run `gradlew assemble` (`./gradlew assemble` if you are on Mac/Linux) 
in the project root. The finalized jar file will be found under the directory "/build/libs"

Source installation information for contributors
-------------------------------------------
MegaOverhaul follows the Minecraft Forge installation methodology. It will apply
some small patches to the vanilla MCP source code, giving you, and itself access
to some data and functions you need to build a successful mod.

Note also that the patches are built against "un-renamed" MCP source code (aka
SRG Names) - this means that you will not be able to read them directly against
normal code.

Setup Process:
==============================

Step 1: Open your command-line and browse to the folder where you extracted the zip file.

Step 2: You're left with a choice.
If you prefer to use Eclipse:
1. Run the following command: `gradlew genEclipseRuns` (`./gradlew genEclipseRuns` if you are on Mac/Linux)
2. Open Eclipse, Import > Existing Gradle Project > Select Folder 
   or run `gradlew eclipse` to generate the project.

If you prefer to use IntelliJ:
1. Open IDEA, and import project.
2. Select your build.gradle file and have it import.
3. Run the following command: `gradlew genIntellijRuns` (`./gradlew genIntellijRuns` if you are on Mac/Linux)
4. Refresh the Gradle Project in IDEA if required.

If at any point you are missing libraries in your IDE, or you've run into problems you can 
run `gradlew --refresh-dependencies` to refresh the local cache. `gradlew clean` to reset everything 
{this does not affect your code} and then start the process again.

Mapping Names:
=============================
MegaOverhaul is configured to use the Parchment mapping names for methods and fields 
in the Minecraft codebase. These names are covered by a specific license. All modders should be aware of this
license. For the latest license text, refer to the mapping file itself, or the reference copy here:
https://github.com/ParchmentMC/Parchment/blob/versions/1.19.x/LICENSE.txt

Additional Resources: 
=========================
Community Documentation: https://mcforge.readthedocs.io/en/latest/gettingstarted/  
LexManos' Install Video: https://www.youtube.com/watch?v=8VEdtQLuLO0  
Forge Forum: https://forums.minecraftforge.net/  
Forge Discord: https://discord.gg/UvedJ9m  
